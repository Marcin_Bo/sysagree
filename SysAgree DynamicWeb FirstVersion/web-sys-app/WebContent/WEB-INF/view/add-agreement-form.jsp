<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>

<title>Save Agreement</title>
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css">

</head>
<body>
	<div id="wrapper">
		<div id="header">
			<h2>Save Agreement</h2>
		</div>
	</div>

	<div>
	<br>
		<form:form action="saveAgreement" modelAttribute="agreement" method="POST">
		<form:hidden path="id" />
			<table>
				<tbody>
					<tr>
						<td><label>Agreement number:</label></td>
						<td><form:input path="agreementNumber" /></td>
					</tr>
					<tr>
						<td><label>System name:</label></td>
						<td><form:input path="sysName" /></td>
					</tr>
					<tr>
						<td><label>Date from:</label></td>
						<td><form:input path="dateFrom" /></td>
					</tr>
					<tr>
						<td><label>Date to:</label></td>
						<td><form:input path="dateTo" /></td>
					</tr>
					<tr>
						<td><label>Agreement price:</label></td>
						<td><form:input path="agreementPrice" /></td>
					</tr>
					<tr>
						<td><label>Amount type:</label></td>
						<td><form:input path="amountType" /></td>
					</tr>
					<tr>
						<td><label>Settlement:</label></td>
						<td><form:input path="settlement" /></td>
					</tr>
					<tr>
						<td><label>Authorization percent:</label></td>
						<td><form:input path="authorizationPercent" /></td>
					</tr>
					<tr>
						<td><label>Active:</label></td>
						<td><form:input path="active" /></td>
					</tr>
					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>
				</tbody>
			</table>
		</form:form>
		
		<div style="clear; both;"></div>
		<p>
			<a href="${pageContext.request.contextPath}/agreements/list">Back to list</a>
		</p>

	</div>
</body>
</html>