<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<title>Index</title>

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>
<body>

	<div id="wrapper">
		<div id="header">
			<h2>Menu</h2>
		</div>
	</div>
	<br>
	<table>
		<tr>
			<th><a href="${pageContext.request.contextPath}/systems/list">Systems list</a></th>
			<th><a href="${pageContext.request.contextPath}/agreements/list">All agreements</a></th>
			<th><a href="${pageContext.request.contextPath}/agreements/activeAgreements">Active agreements</a></th>
			<th><a href="${pageContext.request.contextPath}/application/info">Application info</a></th>
		</tr>
	</table>

</body>
</html>
