package mb.sysapp.service;

import java.util.List;

import mb.sysapp.entity.Systems;

public interface SystemsService {
	
	public List<Systems> getSystems();
	
	public void saveSystem(Systems system);

	public Systems getSystem(int id);

	public void deleteSystem(int id);
}
