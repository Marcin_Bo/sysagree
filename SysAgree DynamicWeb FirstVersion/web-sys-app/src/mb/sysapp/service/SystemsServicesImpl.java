package mb.sysapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mb.sysapp.dao.SystemsDAO;
import mb.sysapp.entity.Systems;

@Service
public class SystemsServicesImpl implements SystemsService {

	@Autowired
	private SystemsDAO systemsDAO;
	
	@Override
	@Transactional
	public List<Systems> getSystems() {
		
		return systemsDAO.getSystems();
	}

	@Override
	@Transactional
	public void saveSystem(Systems system) {
		
		systemsDAO.saveSystem(system);		
	}

	@Override
	@Transactional
	public Systems getSystem(int id) {
		
		return systemsDAO.getSystem(id);
	}

	@Override
	@Transactional
	public void deleteSystem(int id) {
	
		systemsDAO.deleteSystem(id);		
	}
}
