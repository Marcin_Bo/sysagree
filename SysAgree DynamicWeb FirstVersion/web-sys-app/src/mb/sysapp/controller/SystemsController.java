package mb.sysapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mb.sysapp.entity.Systems;
import mb.sysapp.service.SystemsService;

@Controller
@RequestMapping("/systems")
public class SystemsController {
	
	@Autowired
	private SystemsService systemsService;
	
	@GetMapping("/list")
	public String listStystems(Model model) {
		
		List<Systems> systems = systemsService.getSystems();
		
		model.addAttribute("systems", systems);
		
		return "list-systems";
	}
	
	@GetMapping("/addSystemForm")
	public String addSystemForm(Model model) {
		
		Systems system = new Systems();
		
		model.addAttribute("system", system);
		
		return "add-system-form";		
	}
	
	@PostMapping("/saveSystem")
	public String saveSystem(@ModelAttribute("system") Systems system) {
		
		systemsService.saveSystem(system);
		
		return "redirect:/systems/list";
	}

	@GetMapping("/updateSystem")
	public String updateSystem(@RequestParam("systemId") int id, Model model) {
		
		Systems system = systemsService.getSystem(id);
		
		model.addAttribute("system", system);
		
		return "add-system-form";
	}
	
	@GetMapping("/deleteSystem")
	public String deleteSystem(@RequestParam("systemId") int id, Model model) {
		
		systemsService.deleteSystem(id);
		
		return "redirect:/systems/list";
	}
}
