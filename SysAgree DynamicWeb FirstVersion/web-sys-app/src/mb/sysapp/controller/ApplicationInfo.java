package mb.sysapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/application")
public class ApplicationInfo {
	
	@RequestMapping("/info")
	public String applicationInfo() {
		
		return "app-info";
	}
}
