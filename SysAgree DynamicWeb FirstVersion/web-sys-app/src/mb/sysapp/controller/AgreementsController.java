package mb.sysapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mb.sysapp.entity.Agreements;
import mb.sysapp.service.AgreementsService;

@Controller
@RequestMapping("/agreements")
public class AgreementsController {

	@Autowired
	private AgreementsService agreementsService;
	
	@GetMapping("/list")
	public String listAgreements(Model model) {
		
		List<Agreements> agreements = agreementsService.getAgreements();
		
		model.addAttribute("agreements", agreements);
		
		return "list-agreements";
	}
	
	@GetMapping("/addAgreementForm")
	public String addAgreementForm(Model model) {
		
		Agreements agreement = new Agreements();
		
		model.addAttribute("agreement", agreement);
		
		return "add-agreement-form";
	}
	
	@PostMapping("/saveAgreement")
	public String saveAgreement(@ModelAttribute("agreement") Agreements agreement) {
		
		agreementsService.saveAgreement(agreement);
		
		return "redirect:/agreements/list";
	}
	
	@GetMapping("/updateAgreement")
	public String updateAgreement(@RequestParam("agreementId") int id, Model model) {
		
		Agreements agreement = agreementsService.getAgreement(id);
		
		model.addAttribute("agreement", agreement);
		
		return "add-agreement-form";
	}
	
	@GetMapping("/deleteAgreement")
	public String deleteAgreement(@RequestParam("agreementId") int id, Model model) {
		
		agreementsService.deleteAgreement(id);
		
		return "redirect:/agreements/list";
	}
	
	@GetMapping("/activeAgreements")
	public String activeAgreements(Model model) {
		
		List<Agreements> agreements = agreementsService.getActiveAgreements();
		
		model.addAttribute("agreements", agreements);
		
		return "add-agreement-active";
	}
}
