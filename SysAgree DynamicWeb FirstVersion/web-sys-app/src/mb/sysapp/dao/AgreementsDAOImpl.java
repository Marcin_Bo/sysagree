package mb.sysapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mb.sysapp.entity.Agreements;

@Repository
public class AgreementsDAOImpl implements AgreementsDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Agreements> getAgreements() {

		Session currentSession = sessionFactory.getCurrentSession();
		
		Query<Agreements> query = currentSession.createQuery("from Agreements", Agreements.class);
		
		List<Agreements> agreements = query.getResultList();
		
		return agreements;
	}

	@Override
	public void saveAgreement(Agreements agreement) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		currentSession.saveOrUpdate(agreement);
	}

	@Override
	public Agreements getAgreement(int id) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		Agreements agreement = currentSession.get(Agreements.class, id);
		
		return agreement;
	}

	@Override
	public void deleteAgreement(int id) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query query = currentSession.createQuery("delete from Agreements where id=:agreementId");
		
		query.setParameter("agreementId", id);
		
		query.executeUpdate();
	}

	@Override
	public List<Agreements> getActiveAgreements() {

		Session currentSession = sessionFactory.getCurrentSession();
		
		Query<Agreements> query = currentSession.createQuery("from Agreements where active='true'", Agreements.class);
		
		List<Agreements> agreements = query.getResultList();
		
		return agreements;
	}
}
