package mb.sysapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mb.sysapp.entity.Systems;

@Repository
public class SystemsDAOImpl implements SystemsDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Systems> getSystems() {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query<Systems> query = currentSession.createQuery("from Systems", Systems.class);
		
		List<Systems> systems = query.getResultList();
		
		return systems;
	}

	@Override
	public void saveSystem(Systems system) {

		Session currentSession = sessionFactory.getCurrentSession();
		
		currentSession.saveOrUpdate(system);
	}

	@Override
	public Systems getSystem(int id) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		Systems system = currentSession.get(Systems.class, id);
		
		return system;
	}

	@Override
	public void deleteSystem(int id) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query query = currentSession.createQuery("delete from Systems where id=:systemId");
		
		query.setParameter("systemId", id);
		
		query.executeUpdate();
	}
}
