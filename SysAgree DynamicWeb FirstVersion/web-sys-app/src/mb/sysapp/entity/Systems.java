package mb.sysapp.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="systems")
public class Systems {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Id
	@Column(name="sys_name")
	private String sysName;
	@Column(name="sys_info")
	private String sysInfo;
	@Column(name="technology_info")
	private String technologyInfo;
	@Column(name="sys_owner")
	private String sysOwner;
	@OneToMany(mappedBy = "sysName", cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	private Set<Agreements> agreements;

	public Systems() {}

	public Systems(int id, String sysName, String sysInfo, String technologyInfo, String sysOwner) {
		this.id = id;
		this.sysName = sysName;
		this.sysInfo = sysInfo;
		this.technologyInfo = technologyInfo;
		this.sysOwner = sysOwner;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public String getSysInfo() {
		return sysInfo;
	}

	public void setSysInfo(String sysInfo) {
		this.sysInfo = sysInfo;
	}

	public String getTechnologyInfo() {
		return technologyInfo;
	}

	public void setTechnologyInfo(String technologyInfo) {
		this.technologyInfo = technologyInfo;
	}

	public String getSysOwner() {
		return sysOwner;
	}

	public void setSysOwner(String sysOwner) {
		this.sysOwner = sysOwner;
	}
	
	public Set<Agreements> getAgreements() {
		return agreements;
	}

	public void setAgreements(Set<Agreements> agreements) {
		this.agreements = agreements;
	}

	@Override
	public String toString() {
		return "Systems [id=" + id + ", sysName=" + sysName + ", sysInfo=" + sysInfo + ", technologyInfo="
				+ technologyInfo + ", sysOwner=" + sysOwner + "]";
	}
}
