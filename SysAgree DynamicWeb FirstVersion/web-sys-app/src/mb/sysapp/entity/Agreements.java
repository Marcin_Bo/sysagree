package mb.sysapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="agreements")
public class Agreements {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="agreement_number")
	private String agreementNumber;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="sys_name", nullable=false, insertable=false, updatable=false)
	private Systems sysName;
	@Column(name="date_from")
	private String dateFrom;
	@Column(name="date_to")
	private String dateTo;
	@Column(name="agreement_price")
	private float agreementPrice;
	@Column(name="amount_type")
	private String amountType;
	@Column(name="settlement")
	private String settlement;
	@Column(name="authorization_percent")
	private int authorizationPercent;
	@Column(name="active")
	private String active;

	public Agreements() {}

	public Agreements(int id, String agreementNumber, Systems sysName, String dateFrom, String dateTo,
			float agreementPrice, String amountType, String settlement, int authorizationPercent, String active) {
		this.id = id;
		this.agreementNumber = agreementNumber;
		this.sysName = sysName;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.agreementPrice = agreementPrice;
		this.amountType = amountType;
		this.settlement = settlement;
		this.authorizationPercent = authorizationPercent;
		this.active = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgreementNumber() {
		return agreementNumber;
	}

	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	public Systems getSysName() {
		return sysName;
	}

	public void setSysName(Systems sysName) {
		this.sysName = sysName;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public float getAgreementPrice() {
		return agreementPrice;
	}

	public void setAgreementPrice(float agreementPrice) {
		this.agreementPrice = agreementPrice;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getSettlement() {
		return settlement;
	}

	public void setSettlement(String settlement) {
		this.settlement = settlement;
	}

	public int getAuthorizationPercent() {
		return authorizationPercent;
	}

	public void setAuthorizationPercent(int authorizationPercent) {
		this.authorizationPercent = authorizationPercent;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Agreements [id=" + id + ", agreementNumber=" + agreementNumber + ", sysName=" + sysName + ", dateFrom="
				+ dateFrom + ", dateTo=" + dateTo + ", agreementPrice=" + agreementPrice + ", amountType=" + amountType
				+ ", settlement=" + settlement + ", authorizationPercent=" + authorizationPercent + ", active=" + active
				+ "]";
	}
}
