<nav>
	<ul id="menu">
		<br>
		<li><a href="${pageContext.request.contextPath}/">Home</a></li>
		<br>
		<li><a href="${pageContext.request.contextPath}/application/info">Application info</a></li>
		<br>
		<li><a href="${pageContext.request.contextPath}/systems/list">Systems list</a></li>
		<br>
		<li><a href="${pageContext.request.contextPath}/agreements/list">All agreements</a></li>
		<br>
		<li><a href="${pageContext.request.contextPath}/agreements/activeAgreements">Active agreements</a></li>
	</ul>
</nav>