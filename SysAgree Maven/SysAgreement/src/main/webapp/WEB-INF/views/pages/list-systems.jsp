<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
	<title>Systems List</title>
	
	<link type="text/css"
		rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>

	<div id="wrapper">
		<div id="header">
			<h1>Systems List</h1>
		</div>
	</div>
	
	<div id="container">
		<div id="content">
		
		<input type="button" value="Add System"
				onclick="window.location.href='addSystemForm'; return false;"
				class="add-button" />
		
			<table>
				<tr>
					<th>System name</th>
					<th>System info</th>
					<th>Technology info</th>
					<th>System owner</th>
					<th>Update</th>
					<th>Delete</th>
				</tr>
			
				<c:forEach var="tempSystem" items="${systems}">
				
				<c:url var="updateSystem" value="/systems/updateSystem">
					<c:param name="systemId" value="${tempSystem.id}" />
				</c:url>
				
				<c:url var="deleteSystem" value="/systems/deleteSystem">
					<c:param name="systemId" value="${tempSystem.id}" />
				</c:url>
				
					<tr>
						<td>${tempSystem.sysName}</td>
						<td>${tempSystem.sysInfo}</td>
						<td>${tempSystem.technologyInfo}</td>
						<td>${tempSystem.sysOwner}</td>
						<td><a href="${updateSystem}">Update</a>
						<td><a href="${deleteSystem}" onclick="if (!(confirm('Are you sure?'))) return false">Delete</a>
					</tr>
				</c:forEach>
			</table>
			<br>
			<a href="${pageContext.request.contextPath}">Home</a>
		</div>
	</div>
</body>
</html>