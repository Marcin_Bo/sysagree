<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>

<title>Save System</title>
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css">

</head>
<body>
	<div id="wrapper">
		<div id="header">
			<h1>Save System</h1>
		</div>
	</div>

	<div>
	<br>
		<form:form action="saveSystem" modelAttribute="system" method="POST">
		<form:hidden path="id" />
			<table>
				<tbody>
					<tr>
						<td><label>System name:</label></td>
						<td><form:input path="sysName" /></td>
					</tr>
					<tr>
						<td><label>System info:</label></td>
						<td><form:input path="sysInfo" /></td>
					</tr>
					<tr>
						<td><label>Technology info:</label></td>
						<td><form:input path="technologyInfo" /></td>
					</tr>
					<tr>
						<td><label>System owner:</label></td>
						<td><form:input path="sysOwner" /></td>
					</tr>
					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>
				</tbody>
			</table>
		</form:form>
		
		<div style="clear; both;"></div>
		<p>
			<a href="${pageContext.request.contextPath}/systems/list">Back to list</a>
		</p>
	</div>
</body>
</html>