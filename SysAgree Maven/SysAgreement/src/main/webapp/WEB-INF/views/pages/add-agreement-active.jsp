<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
	<title>Active Agreements</title>
	
	<link type="text/css"
		rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>

	<div id="wrapper">
		<div id="header">
			<h1>Active Agreements</h1>
		</div>
	</div>
	
	<div id="container">
		<div id="content">
		
			<table>
				<tr>
					<th>Agreement number</th>
					<th>System name</th>
					<th>Date from</th>
					<th>Date to</th>
					<th>Agreement price</th>
					<th>Settlement</th>
				</tr>
			
				<c:forEach var="tempAgreement" items="${agreements}">		
					<tr>
						<td>${tempAgreement.agreementNumber}</td>
						<td>${tempAgreement.system.sysName}</td>
						<td>${tempAgreement.dateFrom}</td>
						<td>${tempAgreement.dateTo}</td>
						<td>${tempAgreement.agreementPrice}</td>
						<td>${tempAgreement.settlement}</td>
					</tr>
				</c:forEach>
			</table>
			<br>
			<a href="${pageContext.request.contextPath}">Back to menu</a>
		</div>
	</div>
</body>
</html>