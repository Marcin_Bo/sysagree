<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
	<title>All Agreements</title>
	
	<link type="text/css"
		rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>

	<div id="wrapper">
		<div id="header">
			<h1>All Agreements</h1>
		</div>
	</div>
	
	<div id="container">
		<div id="content">
		
		<input type="button" value="Add Agreement"
				onclick="window.location.href='addAgreementForm'; return false;"
				class="add-button" />
		
			<table>
				<tr>
					<th>Agreement number</th>
					<th>System name</th>
					<th>Date from</th>
					<th>Date to</th>
					<th>Agreement price</th>
					<th>Settlement</th>
					<th>Update</th>
					<th>Delete</th>
				</tr>
			
				<c:forEach var="tempAgreement" items="${agreements}">
				
				<c:url var="updateAgreement" value="/agreements/updateAgreement">
					<c:param name="agreementId" value="${tempAgreement.id}" />
				</c:url>
				
				<c:url var="deleteAgreement" value="/agreements/deleteAgreement">
					<c:param name="agreementId" value="${tempAgreement.id}" />
				</c:url>
				
					<tr>
						<td>${tempAgreement.agreementNumber}</td>
						<td>${tempAgreement.system.sysName}</td>
						<td>${tempAgreement.dateFrom}</td>
						<td>${tempAgreement.dateTo}</td>
						<td>${tempAgreement.agreementPrice}</td>
						<td>${tempAgreement.settlement}</td>
						<td><a href="${updateAgreement}">Update</a>
						<td><a href="${deleteAgreement}" onclick="if (!(confirm('Are you sure?'))) return false">Delete</a>
					</tr>
				</c:forEach>
			</table>
			<br>
			<a href="${pageContext.request.contextPath}">Back to menu</a>
		</div>
	</div>
</body>
</html>