package mb.sysapp.test.controller;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import mb.sysapp.controller.AgreementsController;
import mb.sysapp.entity.Agreements;
import mb.sysapp.service.AgreementsService;

@RunWith(MockitoJUnitRunner.class)
public class SystemsControllerTest {

	@Mock
	Model mockModel;
	
	@Mock
	AgreementsService agreementService;
	
	@InjectMocks
	AgreementsController agreementsController;
	
	@Test
	public void shouldAddAgreementsToModel() {
		
		List<Agreements> agreements = new LinkedList<>();
		
		Mockito.when(agreementService.getAgreements()).thenReturn(agreements);
		
		String result = agreementsController.listAgreements(mockModel);
		
		ArgumentCaptor<String> argument1 = ArgumentCaptor.forClass(String.class);
		
		ArgumentCaptor<List> argument2 = ArgumentCaptor.forClass(List.class);
		
		Mockito.verify(mockModel).addAttribute(argument1.capture(), argument2.capture());   // sprawdzenie czy metoda zosta�a wywo�na
		
		Assert.assertEquals("list-agreements", result);
		Assert.assertSame(agreements, argument2.getValue());  // zwraca "=="
	}
	
	@Test
	public void shouldAddAgreementFormReturnCorrectViewName() {
			
		String result = agreementsController.addAgreementForm(mockModel);
				
		Assert.assertEquals("add-agreement-form", result);
	}
	
	@Test
	public void shouldAddAgreementFormAddNewAgreementToModel() {
		
		agreementsController.addAgreementForm(mockModel);
		
		ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
		
		Mockito.verify(mockModel, Mockito.times(1)).addAttribute(argument.capture(), Mockito.any(Agreements.class));
	
		Assert.assertEquals("agreement", argument.getValue());
	}

	// nast�pnie sprawdzi� czy metoda update zwraca "add-agreement-form"
	
//	@Test
//	public void shouldSaveAgreementFormReturnCorrectViewName() {
//		
//		agrr
//		
//		String result = agreementsController.saveAgreement();
//				
//		Assert.assertEquals("add-agreement-form", result);
//	}
	
}
