package mb.sysapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import mb.sysapp.entity.Agreements;
import mb.sysapp.entity.Systems;
import mb.sysapp.service.AgreementsService;
import mb.sysapp.service.SystemsService;

@Controller
@RequestMapping("/agreements")
public class AgreementsController {

	@Autowired
	private AgreementsService agreementsService;
	
	@Autowired
	private SystemsService systemsService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listAgreements(Model model) {

		List<Agreements> agreements = agreementsService.getAgreements();

		model.addAttribute("agreements", agreements);

		return "list-agreements";
	}

	@RequestMapping(value = "/addAgreementForm", method = RequestMethod.GET)
	public String addAgreementForm(Model model) {

		Agreements agreement = new Agreements();
		List<Systems> systems = systemsService.getSystems();
		
		model.addAttribute("agreement", agreement);
		agreement.setSystemId(systems.get(0).getId());
		model.addAttribute("availableSystems", systems);
		
		return "add-agreement-form";
	}

	@RequestMapping(value = "/saveAgreement", method = RequestMethod.POST)
	public String saveAgreement(@ModelAttribute("agreement") Agreements agreement) {

		Systems system = systemsService.getSystem(agreement.getSystemId());
		
		agreement.setSystem(system);
		
		agreementsService.saveAgreement(agreement);

		return "redirect:/agreements/list";
	}

	@RequestMapping(value = "/updateAgreement", method = RequestMethod.GET)
	public String updateAgreement(@RequestParam("agreementId") int id, Model model) {

		Agreements agreement = agreementsService.getAgreement(id);
		agreement.setSystemId(agreement.getSystem().getId());
		model.addAttribute("agreement", agreement);
		
		model.addAttribute("availableSystems", systemsService.getSystems());

		return "add-agreement-form";
	}

	@RequestMapping(value = "/deleteAgreement", method = RequestMethod.GET)
	public String deleteAgreement(@RequestParam("agreementId") int id, Model model) {

		// set ative false
		
		agreementsService.deleteAgreement(id);

		return "redirect:/agreements/list";
	}

	@RequestMapping(value = "/activeAgreements", method = RequestMethod.GET)
	public String activeAgreements(Model model) {

		List<Agreements> agreements = agreementsService.getActiveAgreements();

		model.addAttribute("agreements", agreements);

		return "add-agreement-active";
	}
}
