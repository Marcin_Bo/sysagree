package mb.sysapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/application")
public class ApplicationInfo {
	
	@RequestMapping(value="/info", method = RequestMethod.GET)
	public String applicationInfo() {
		
		return "app-info";
	}
}
