package mb.sysapp.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;

import mb.sysapp.entity.Systems;

@Repository
public class SystemsDAOImpl implements SystemsDAO {

	@Autowired
	private LocalSessionFactoryBean sessionFactory;
	
	@Override
	public List<Systems> getSystems() {
		
		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		Query query = currentSession.createQuery("from Systems");
		
		List<Systems> systems = query.list();
		
		return systems;
	}

	@Override
	public void saveSystem(Systems system) {

		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		currentSession.saveOrUpdate(system);
	}

	@Override
	public Systems getSystem(int id) {
		
		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		Systems system = (Systems) currentSession.get(Systems.class, id);
		
		return system;
	}

	@Override
	public void deleteSystem(int id) {
		
		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		Query query = currentSession.createQuery("delete from Systems where id=:systemId");
		
		query.setParameter("systemId", id);
		
		query.executeUpdate();
	}
}
