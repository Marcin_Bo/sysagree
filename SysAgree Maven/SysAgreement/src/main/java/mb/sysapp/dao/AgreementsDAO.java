package mb.sysapp.dao;

import java.util.List;

import mb.sysapp.entity.Agreements;

public interface AgreementsDAO {

	public List<Agreements> getAgreements();

	public void saveAgreement(Agreements agreement);

	public Agreements getAgreement(int id);

	public void deleteAgreement(int id);

	public List<Agreements> getActiveAgreements();
}
