package mb.sysapp.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;

import mb.sysapp.entity.Agreements;

@Repository
public class AgreementsDAOImpl implements AgreementsDAO {

	@Autowired
	private LocalSessionFactoryBean sessionFactory;
	
	@Override
	public List<Agreements> getAgreements() {

		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		Query query = currentSession.createQuery("from Agreements");
		
		List<Agreements> agreements = query.list();
		
		return agreements;
	}

	@Override
	public void saveAgreement(Agreements agreement) {
		
		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		currentSession.saveOrUpdate(agreement);
	}

	@Override
	public Agreements getAgreement(int id) {
		
		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		Agreements agreement = (Agreements) currentSession.get(Agreements.class, id);
		
		return agreement;
	}

	@Override
	public void deleteAgreement(int id) {
		
		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		Query query = currentSession.createQuery("delete from Agreements where id=:agreementId");
		
		query.setParameter("agreementId", id);
		
		query.executeUpdate();
	}

	@Override
	public List<Agreements> getActiveAgreements() {

		Session currentSession = sessionFactory.getObject().getCurrentSession();
		
		Query query = currentSession.createQuery("from Agreements where active='true'"); //Query query = currentSession.createQuery("from Agreements where active='true' and now()>date_from and now() < date_to");
		
		List<Agreements> agreements = query.list();
		
		return agreements;
	}
}
