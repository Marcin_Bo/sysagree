package mb.sysapp.dao;

import java.util.List;

import mb.sysapp.entity.Systems;

public interface SystemsDAO {

	public List<Systems> getSystems();

	public void saveSystem(Systems system);

	public Systems getSystem(int id);

	public void deleteSystem(int id); 
}
