package mb.sysapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mb.sysapp.dao.AgreementsDAO;
import mb.sysapp.entity.Agreements;

@Service
public class AgreementsServiceImpl implements AgreementsService {

	@Autowired
	private AgreementsDAO agreementsDAO;
	
	@Override
	@Transactional
	public List<Agreements> getAgreements() {
		
		return agreementsDAO.getAgreements();
	}

	@Override
	@Transactional
	public void saveAgreement(Agreements agreement) {
		
		agreementsDAO.saveAgreement(agreement);
	}

	@Override
	@Transactional
	public Agreements getAgreement(int id) {
		
		return agreementsDAO.getAgreement(id);
	}

	@Override
	@Transactional
	public void deleteAgreement(int id) {

		agreementsDAO.deleteAgreement(id);		
	}

	@Override
	@Transactional
	public List<Agreements> getActiveAgreements() {
		
		return agreementsDAO.getActiveAgreements();
	}
}
