DROP SCHEMA IF EXISTS sysapp;

CREATE DATABASE sysapp;

USE sysapp;

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS sysapp.systems;

CREATE TABLE sysapp.systems (
id int(11) NOT NULL AUTO_INCREMENT,
sys_name varchar(50) NOT NULL,
sys_info varchar(150) DEFAULT NULL,
technology_info varchar(150) DEFAULT NULL,
sys_owner varchar(150) DEFAULT NULL,
UNIQUE KEY (sys_name),
PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS sysapp.agreements;

CREATE TABLE sysapp.agreements (
id int(11) NOT NULL AUTO_INCREMENT,
agreement_number varchar(50) DEFAULT NULL,
system_id int(11) NOT NULL,
date_from varchar(20) DEFAULT NULL,
date_to varchar(20) DEFAULT NULL,
agreement_price DECIMAL(7,2) DEFAULT NULL,
amount_type varchar(3) DEFAULT NULL,
settlement varchar(20) DEFAULT NULL,
authorization_percent int(3) DEFAULT NULL,
active varchar(5) DEFAULT NULL,
PRIMARY KEY (id),
CONSTRAINT FK_DEPT FOREIGN KEY (system_id) REFERENCES sysapp.systems (id)
ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO sysapp.systems VALUES
	(1,'KUCYK','maly konik','Java','Janek'),
	(2,'LODKA','do plywania','C++','Franek'),
	(3,'KAPISZON','takie sobie','.NET','Zdzichu'),
	(4,'KOTEK','miauu','C#','Staszek'),
    (5,'DEMON','straszne','Ruby','Stefan'),
    (6,'ZOLWIK','skorupa','Python','Gutek'),
    (7,'KOJOTEK','na strusie','GO','Ziutek');

INSERT INTO sysapp.agreements VALUES
	(1,'22/2011',1,'2012-02-02','2014-04-03',100.00,'NET','MONTH',2,'false'),
	(2,'21/2012',2,'2012-03-10','2014-03-03',555.00,'NET','MONTH',2,'false'),
	(3,'34/2010',3,'2011-06-04','2014-06-05',453.33,'NET','MONTH',2,'false'),
	(4,'22/2015',4,'2015-01-21','2016-01-01',123.32,'NET','MONTH',2,'false'),
    (5,'303/2017',5,'2017-03-15','2014-20-22',122.11,'BRU','YEAR',2,'false'),
    (6,'212/2017',1,'2017-01-23','2017-12-25',122.12,'NET','MONTH',2,'true'),
	(7,'311/2017',3,'2017-01-12','2017-12-31',444.00,'BRU','YEAR',2,'true');

SET FOREIGN_KEY_CHECKS = 1;
